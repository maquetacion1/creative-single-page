(function( $ ) {
    $(document).ready(function (){

        let mainNavigation= document.getElementsByClassName("main-navigation");
        let menutoggle = document.getElementsByClassName("menu-toggle");

        // Menu de navegacion
        $(document).on('click','.menu-toggle',function(event){
            event.preventDefault();
            // Cuando el menu se cierra
            if(mainNavigation[0].classList.contains("open")){
                menutoggle[0].innerHTML =
                '<img src="./assets/mobile/icon-hamburger.svg" alt="close menu button" />';
                $(".main-navigation").slideUp( 'fast', function(){ 
                    mainNavigation[0].classList.remove("open");
                });
            } // Cuando abrimos el menu
            else {
                menutoggle[0].innerHTML =
                '<img src="./assets/mobile/icon-cross.svg" alt="close menu button" />';
                $(".main-navigation").slideDown( 'fast', function(){ 
                    $(".main-navigation").css("display",'grid');
                    mainNavigation[0].className += " open";
                });
            }
        });

        // Carrousel
        var slideIndex = 1;
        showDivs(slideIndex);

        function plusDivs(n) {
        showDivs(slideIndex += n);
        }

        function showDivs(n) {
        var i;
        var x = document.getElementsByClassName("entry-project");
        if (n > x.length) {slideIndex = 1}
        if (n < 1) {slideIndex = x.length} ;
        for (i = 0; i < x.length; i++) {
            x[i].style.display = "none";
        }
        x[slideIndex-1].style.display = "grid";
        }

        $(document).on('click','.entry-button-left',function(event){
            plusDivs(-1);
        }),
        $(document).on('click','.entry-button-right',function(event){
            plusDivs(+1);
        })
    })
})( jQuery );